Web application written in Go.

It is based on Davy Wybiral tutorials: https://github.com/code-tutorials/golang-webapp

Application requires Redis database.