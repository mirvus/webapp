package main

import (
	"net/http"

	"gitlab.com/mirvus/webapp/models"
	"gitlab.com/mirvus/webapp/routes"
	"gitlab.com/mirvus/webapp/utils"
)

func main() {
	models.Init()
	utils.LoadTemplates("templates/*.html")
	r := routes.NewRouter()
	http.Handle("/", r)
	http.ListenAndServe(":8080", nil)
}
